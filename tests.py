#!/bin/python3

import unittest
from hypothesis import assume, given, note, settings
from hypothesis.extra.numpy import arrays
from hypothesis.strategies import data, floats, integers, tuples

import numpy as np
from numpy import linalg as la
import svd

shapes = tuples(integers(1, 10), integers(1, 10))

class OrthogonalTransformation:
    def apply_transform(self, M, data):
        random = self.draw(data)(M.shape)
        return self.transform(random)(M)

    @given(arrays(np.float, shapes), data())
    def test_leaves_shape_intact(self, M, data):
        res = self.apply_transform(M, data)
        self.assertEqual(type(res), type(M))
        self.assertEqual(res.shape, M.shape)

    @given(arrays(np.float, shapes, elements=floats(allow_nan=False,
        allow_infinity=False, width=16)), data())
    def test_leaves_norm_intact(self, M, data):
        res = self.apply_transform(M, data)
        self.assertAlmostEqual(la.norm(res), la.norm(M))

    @given(arrays(np.float, shapes, elements=floats(allow_nan=False,
        allow_infinity=False, width=16)), data())
    def test_kills(self, M, data):
        # Same as apply_transform above, but now we need access to random, in
        # order to pass it to kills_pred
        random = self.draw(data)(M.shape)
        res = self.transform(random)(M)
        for i in range(0, res.shape[0]):
            for j in range(0, res.shape[1]):
                if self.kills_pred(random)((i, j)):
                    self.assertAlmostEqual(res[i, j], 0,
                            msg = "entry %d, %d" % (i, j))

class TestHouseholder(OrthogonalTransformation, unittest.TestCase):
    def setUp(self):
        def appl_householder(M):
            A, P = svd.householder(M.copy())
            return P.dot(M)
            # return A
        self.draw = lambda _: lambda _: None
        self.transform = lambda data: appl_householder
        self.kills_pred = lambda _: lambda t: t[0] > 0 and t[1] == 0

class TestBidiagonalization(OrthogonalTransformation, unittest.TestCase):
    def setUp(self):
        def appl_bidiagonalize(M):
            A, P, Q = svd.bidiagonalize(M.copy())
            return P.dot(M).dot(Q)
            # return A
        self.draw = lambda _: lambda _: None
        self.transform = lambda data: appl_bidiagonalize
        self.kills_pred = lambda _: lambda t: t[0] != t[1] and t[0] + 1 != t[1]

class TestGivens(OrthogonalTransformation, unittest.TestCase):
    def setUp(self):
        def appl_givens(M, k, l):
            A, O = svd.givens(M.copy(), k, l)
            return O.dot(M)
            # return A
        self.draw = lambda data: lambda shape: \
                data.draw(tuples(integers(0, shape[0] - 1),
                    integers(0, shape[1] - 1)).filter(lambda t: t[0] > t[1]))
        self.transform = lambda t: lambda M: appl_givens(M, t[0], t[1])
        self.kills_pred = lambda drawn: lambda candidate: drawn == candidate

class TestQRStep(unittest.TestCase):
    @given(arrays(np.float, tuples(integers(5, 10), integers(5, 10)),
        elements=floats(allow_nan=False, allow_infinity=False, width=16)))
    def test_transformation_matrices_match_result(self, M):
        M, _, _ = svd.bidiagonalize(M)
        note(M)
        A, P, Q = svd.qr_step(M.copy())
        contending = P.dot(M).dot(Q)
        for i in range(A.shape[0]):
            for j in range(A.shape[1]):
                self.assertAlmostEqual(A[i, j], contending[i, j])

class TestSVD(unittest.TestCase):
    @given(arrays(np.float, tuples(integers(5, 10), integers(5, 10)),
        elements=floats(allow_nan=False, allow_infinity=False, width=16)))
    @settings(deadline=None)
    def test_transformation_matrices_match_result(self, M):
        A, P, Q, _ = svd.singular_value_decomposition(M.copy())
        contending = P.dot(M).dot(Q)
        for i in range(A.shape[0]):
            for j in range(A.shape[1]):
                self.assertAlmostEqual(A[i, j], contending[i, j])

    @given(arrays(np.float, tuples(integers(5, 10), integers(5, 10)),
        elements=floats(allow_nan=False, allow_infinity=False, width=16)))
    @settings(deadline=None)
    def test_produces_diagonal(self, M):
        A, P, Q, i = svd.singular_value_decomposition(M.copy(), maxiter=100)
        assume(i != 100)
        for i in range(A.shape[0]):
            for j in range(A.shape[1]):
                if i != j:
                    self.assertAlmostEqual(A[i, j], 0)

if __name__ == '__main__':
    unittest.main()
