#!/bin/python3

# Implementation of the Singular Value Decomposition, as described in:
#   P. Deuflhard and A. Hohmann. Numerische Mathematik. 1. de Gruyter
#   Lehrbuch. Walter de Gruyter & Co., Berlin, fourth edition, 2008. Eine
#   algorithmisch orientierte Einführung.
# (Refer to Algorithm 5.22.)

# Skeleton of the SVD algorithm implemented here:
# Input: A ∈ Mat_{m,n}(ℝ)
# * Transform A to become bidiagonal, using orthogonal transformations P ∈ O(m),
#   Q ∈ O(n), more specifically: using householder reflections.  This yields
#     P A Q = (B)
#             (0),  B ∈ Mat_n(ℝ) upper bidiagonal.
# * Apply the QR-Algorithm for eigenvalues to Bᵀ B, without actually computing
#   that matrix, but instead only operating on B itself.  (Computing Bᵀ B is
#   poorly conditioned.)

# Skeleton of the QR-Algorithm for eigenvalues implemented here:
# Input: B ∈ Mat_n(ℝ) upper bidiagonal
# Repeat the following until B is almost diagonal:
#   Repeat the following, chasing across the diagonal
#   * Apply a givens rotation (from the right) to B
#     (Creates a fill-in element, i.e. B is not upper bidiagonal anymore)
#   * Apply a givens rotation (from the left) to B
#     (Removes the above fill-in element, but creates a new one, which is then
#     removed by the above bullet point in the next iteration)
#   (In the end, once we're at the bottom right, B is upper bidiagonal again.)

import numpy as np
from numpy import linalg as la
import math

progress_bars = False

# If there's tqdm, use it to show progress bars;  if not, don't show them.
try:
    from tqdm import tqdm as tqdm_
except ModuleNotFoundError:
    def tqdm_(it, **kwargs):
        return it

def tqdm(it, **kwargs):
    global progress_bars

    if progress_bars:
        return tqdm_(it, **kwargs)
    else:
        return it

def householder(M, extra_matrices = []):
    # Notice that M will be only a part of the entire current matrix as seen by
    # the bidiagonalization process.  This means that here, we always want to
    # eliminate the top left entry of M.  By transposing appropriately, we only
    # need to implement the elimination from the left.
    """ Given a matrix M, compute an orthogonal matrix P such that the leftmost
    column of P M has zeroes below its topmost entry. If any matrices are given
    in extra_matrices, the householder transformation is applied to them,
    mutating the matrices. """

    if M.dtype != 'float64':
        raise Exception("Entry data type should be float64, not %s" % M.dtype)

    a = M[:, 0]
    alpha = - math.copysign(1, a[0]) * la.norm(a)
    if abs(alpha) < 1e-10:
        return M, np.eye(M.shape[0])

    v = a.copy()
    v[0] -= alpha
    divisor = (alpha * (a[0] - alpha))

    P = np.eye(M.shape[0]) + np.outer(v, v) / divisor

    M[0, 0] = alpha
    M[1:, 0] = np.zeros(M.shape[0] - 1)

    for i in range(1, M.shape[1]):
        M[:, i] = M[:, i] + (np.dot(v, M[:, i]) / divisor) * v

    for m in extra_matrices:
        for i in range(0, m.shape[1]):
            m[:, i] = m[:, i] + (np.dot(v, m[:, i]) / divisor) * v

    return M, P

def bidiagonalize(A):
    # Note that the B returned here does not perfectly correspond to the B in
    # the skeleton of the SVD algorithm.  The actual B here contains the zeros
    # at the bottom.
    """ Compute P ∈ O(m), Q ∈ O(n), such that P A Q is upper bidiagonal. """

    if A.dtype != 'float64':
        raise Exception("Entry data type should be float64, not %s" % A.dtype)

    P = np.eye(A.shape[0])
    Q = np.eye(A.shape[1])

    for j in tqdm(range(min(A.shape)), desc='bidiagonalize'):
        if A.shape[0] > j + 1:
            A[j:,j:], _ = householder(A[j:,j:], [P[j:,:j], P[j:,j:]])

        if A.shape[1] > j + 1:
            A2, _ = householder(A[j:,j+1:].transpose(), [Q[j+1:,:j+1], Q[j+1:,j+1:]])
            A[j:,j+1:] = A2.transpose()

    return A, P, Q.transpose()

def givens(M, k, l, extra_matrices = []):
    """ Compute an orthogonal matrix Ω such that the (k,l)-th entry of Ω M
    is zero. If any matrices are given in extra_matrices, the givens rotation is
    applied to them, mutating the matrices. """

    if M.dtype != 'float64':
        raise Exception("Entry data type should be float64, not %s" % M.dtype)

    if abs(M[k, l]) < 1e-10:
        return M, np.eye(M.shape[0])

    x = M[:, l]
    if abs(x[l]) > abs(x[k]):
        tau = x[k] / x[l]
        s = 1 / math.sqrt(1 + tau ** 2)
        c = s * tau
    else:
        tau = x[l] / x[k]
        c = 1 / math.sqrt(1 + tau ** 2)
        s = c * tau

    # Interestingly, this is not exactly the same as in section 3.2.1. of
    # Deuflhard/Hohmann (cited at the beginning of this source file);  I had to
    # swap k/l in the first argument of [] brackets in each of the lines below.
    Omega = np.eye(M.shape[0])
    Omega[l, k] = c
    Omega[l, l] = s
    Omega[k, k] = -s
    Omega[k, l] = c

    # Written as one statement so that M[k, :] doesn't need to be copied first
    M[k,:], M[l,:] = Omega[k, k] * M[k,:] + Omega[k, l] * M[l,:], \
            Omega[l, l] * M[l,:] + Omega[l, k] * M[k,:]

    for m in extra_matrices:
        m[k,:], m[l,:] = Omega[k, k] * m[k,:] + Omega[k, l] * m[l,:], \
                Omega[l, l] * m[l,:] + Omega[l, k] * m[k,:]

    return M, Omega

def qr_step(M, P = None, Q = None):
    """ Perform one step of the QR algorithm on M, i.e. compute P ∈ O(m), Q ∈
    O(n) such that P M Q is still upper bidiagonal, but one step closer to the
    diagonal matrix of singular values.  Return P M Q, P, Q.
    """

    if M.dtype != 'float64':
        raise Exception("Entry data type should be float64, not %s" % M.dtype)

    if type(P) == type(None):
        P = np.eye(M.shape[0])

    if type(Q) == type(None):
        Q = np.eye(M.shape[1])
    else:
        Q = Q.transpose()

    for i in tqdm(range(min(M.shape)), desc='qr_step'):
        M, _ = givens(M.transpose(), i, i - 1, [Q])
        M, _ = givens(M.transpose(), i, i - 1, [P])

    return M, P, Q.transpose()

def qr_algorithm(M, tol=1e-10, maxiter=100):
    """ Perform the QR algorithm on M, until the error, as measured by the
    largest absolute of an entry on the 1-diagonal (i.e. the second diagonal,
    that will vanish via continued application of qr_step), becomes smaller than
    tol, or maxiter iterations have been done.  Return an approximation of Λ,
    the diagonal matrix of singular values of M, along with P ∈ O(m), Q ∈ O(n)
    orthogonal matrices such that M = P Λ Q.  Return a 4-tuple with these
    objects and the number of iterations after which the algorithm
    terminated. """

    if M.dtype != 'float64':
        raise Exception("Entry data type should be float64, not %s" % M.dtype)

    P = np.eye(M.shape[0])
    Q = np.eye(M.shape[1])

    err = float('inf')

    for i in tqdm(range(maxiter), desc='qr_algorithm'):
        if err < tol:
            return M, P, Q, i

        M, P, Q = qr_step(M, P, Q)

        err = la.norm(M.diagonal(1), ord=float('inf'))

    return M, P, Q, maxiter

def singular_value_decomposition(M, tol=1e-10, maxiter=100):
    if M.dtype != 'float64':
        raise Exception("Entry data type should be float64, not %s" % M.dtype)
    A, P, Q = bidiagonalize(M)
    A, P2, Q2, reason = qr_algorithm(A, tol, maxiter)
    return A, P2.dot(P), Q.dot(Q2), reason
